﻿using MedicineWarehouse.Domain.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineWarehouse.Infrastructure.Services
{
    public interface IAuthService
    {
        public Task<AuthDto> RegisterAsync(RegisterDto dto);
        public Task<AuthDto> LoginAsync(LoginDto dto);
        public Task<string> AssignRoleAsync(AssignRoleDto dto);
    }
}
