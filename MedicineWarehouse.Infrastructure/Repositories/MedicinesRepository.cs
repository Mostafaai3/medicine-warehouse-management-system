﻿using MedicineWarehouse.Domain.DTOs;
using MedicineWarehouse.Domain.Interfaces;
using MedicineWarehouse.Domain.Models;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace MedicineWarehouse.Infrastructure.Repositories
{
    public class MedicinesRepository : IMedicinesRepository
    {
        private readonly IConfiguration _configuration;
        public MedicinesRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<string> Delete(int id)
        {
            var outParam = new SqlParameter("@ReturnCode", SqlDbType.NVarChar, 20)
            {
                Direction = ParameterDirection.Output
            };
            SqlParameter[] parameters = {
                new SqlParameter("@Id",id),
                outParam
            };
            using (var sqlConnection = new SqlConnection(_configuration.GetConnectionString("SqlCon")))
            {
                using (var command = sqlConnection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "DeleteMedicine";
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    await sqlConnection.OpenAsync();
                    await command.ExecuteScalarAsync();
                }
            }
            return outParam.Value.ToString();
        }

        public Task<List<MedicineDetailsDto>> Get()
        {
            var medicines = LoadMedicinesList();
            return Task.FromResult(medicines.ToList());
        }

        public Task<MedicineDetailsDto> GetById(int id)
        {
            var medicines = LoadMedicinesList();
            var medicine = medicines.FirstOrDefault(p => p.Id == id);
            return Task.FromResult(medicine);
        }

        public Task<MedicineDetailsDto> GetByCode(string code)
        {
            var medicines = LoadMedicinesList();
            var medicine = medicines.FirstOrDefault(p => p.Barcode == code);
            return Task.FromResult(medicine);
        }

        public Task<List<MedicineDetailsDto>> Search(string? term = null, int? categoryId = null, int? companyId = null, int? scientificNameId = null)
        {
            var medicines = LoadMedicinesList();
            if (term is not null)
            {
                medicines = medicines.Where(m=> m.Name.Contains(term));
            }
            if (categoryId is not null)
            {
                medicines = medicines.Where(m => m.CategoryId == categoryId);
            }
            if (companyId is not null)
            {
                medicines = medicines.Where(m => m.CompanyId == companyId);
            }
            if (scientificNameId is not null)
            {
                medicines = medicines.Where(m => m.ScientificNameId == scientificNameId);
            }
            return Task.FromResult(medicines.ToList());
        }

        public async Task<string> Save(Medicine medicine)
        {
            var outParam = new SqlParameter("@ReturnCode", SqlDbType.NVarChar, 20)
            {
                Direction = ParameterDirection.Output
            };
            SqlParameter[] parameters = {
                new SqlParameter("@Id",medicine.Id),
                new SqlParameter("@Name",medicine.Name),
                new SqlParameter("@Price",medicine.Price),
                new SqlParameter("@Barccode",medicine.Barcode),
                new SqlParameter("@CompanyId",medicine.CompanyId),
                new SqlParameter("@CategoryId",medicine.CategoryId),
                new SqlParameter("@ScientificNameId",medicine.ScientificNameId),
                outParam
            };
            using (var sqlConnection = new SqlConnection(_configuration.GetConnectionString("SqlCon")))
            {
                using (var command = sqlConnection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "SaveMedicine";
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    await sqlConnection.OpenAsync();
                    await command.ExecuteScalarAsync();
                }
            }
            return outParam.Value.ToString();
        }

        public async Task<string> SupplyMedicine(SupplyMedicineDto dto)
        {
            var outParam = new SqlParameter("@ReturnCode", SqlDbType.NVarChar, 20)
            {
                Direction = ParameterDirection.Output
            };
            SqlParameter[] parameters = {
                new SqlParameter("@MedicineId",dto.MedicineId),
                new SqlParameter("@Quantity",dto.Quantity),
                new SqlParameter("@ExpirationDate",dto.ExpirationDate),
                outParam
            };
            using (var sqlConnection = new SqlConnection(_configuration.GetConnectionString("SqlCon")))
            {
                using (var command = sqlConnection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "SupplyMedicine";
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    await sqlConnection.OpenAsync();
                    await command.ExecuteScalarAsync();
                }
            }
            return outParam.Value.ToString();
        }

        public Task<List<Group>> GetMedicineGroups(int id)
        {
            var groups = LoadGroupsList();
            var medicineGroups = groups.Where(g => g.MedicineId == id && g.Quantity > 0).ToList();
            return Task.FromResult(medicineGroups);
        }

        public IEnumerable<MedicineDetailsDto> LoadMedicinesList()
        {
            List<MedicineDetailsDto> medicines = new List<MedicineDetailsDto>();

            using SqlConnection con = new SqlConnection(_configuration.GetConnectionString("SqlCon"));
            con.Open();
            using SqlCommand cmd = new SqlCommand("execute GetMedicineDetails", con);
            using SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                MedicineDetailsDto medicine = new MedicineDetailsDto
                {
                    Id = Convert.ToInt32(row["Id"]),
                    Name = row["Name"].ToString(),
                    Price = Convert.ToInt32(row["Price"]),
                    Barcode = row["Barcode"].ToString(),
                    ScientificNameId = Convert.ToInt32(row["ScientificNameId"]),
                    CompanyId = Convert.ToInt32(row["CompanyId"]),
                    CategoryId = Convert.ToInt32(row["CategoryId"]),
                    ScientificName = row["ScientificName"].ToString(),
                    Company = row["Company"].ToString(),
                    Category = row["Category"].ToString(),
                };

                medicines.Add(medicine);
            }
            return medicines;
        }

        public IEnumerable<Group> LoadGroupsList()
        {
            List<Group> groups = new List<Group>();

            using SqlConnection con = new SqlConnection(_configuration.GetConnectionString("SqlCon"));
            con.Open();
            using SqlCommand cmd = new SqlCommand("select * from groups", con);
            using SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                Group group = new Group
                {
                    Id = Convert.ToInt32(row["Id"]),
                    Quantity = Convert.ToInt32(row["Quantity"]),
                    MedicineId = Convert.ToInt32(row["MedicineId"]),
                    ExpirationDate = Convert.ToDateTime(row["ExpirationDate"])
                };

                groups.Add(group);
            }
            return groups;
        }
    }
}
