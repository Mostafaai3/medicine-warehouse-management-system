﻿using MedicineWarehouse.Domain.Interfaces;
using MedicineWarehouse.Domain.Models;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineWarehouse.Infrastructure.Repositories
{
    public class CategoriesRepository : ICategoriesRepository
    {
        private readonly IConfiguration _configuration;
        public CategoriesRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task<string> Delete(int id)
        {
            var outParam = new SqlParameter("@ReturnCode", SqlDbType.NVarChar, 20)
            {
                Direction = ParameterDirection.Output
            };
            SqlParameter[] parameters = {
                new SqlParameter("@Id",id),
                outParam
            };
            using (var sqlConnection = new SqlConnection(_configuration.GetConnectionString("SqlCon")))
            {
                using (var command = sqlConnection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "DeleteCategory";
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    await sqlConnection.OpenAsync();
                    await command.ExecuteScalarAsync();
                }
            }
            return outParam.Value.ToString();
        }

        public Task<List<Category>> Get()
        {
            var categories = LoadCategoriesList();
            return Task.FromResult(categories.ToList());
        }

        public async Task<string> Save(Category category)
        {
            var outParam = new SqlParameter("@ReturnCode", SqlDbType.NVarChar, 20)
            {
                Direction = ParameterDirection.Output
            };
            SqlParameter[] parameters = {
                new SqlParameter("@Id",category.Id),
                new SqlParameter("@Name",category.Name),
                outParam
            };
            using (var sqlConnection = new SqlConnection(_configuration.GetConnectionString("SqlCon")))
            {
                using (var command = sqlConnection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "SaveCategory";
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    await sqlConnection.OpenAsync();
                    await command.ExecuteScalarAsync();
                }
            }
            return outParam.Value.ToString();
        }

        public IEnumerable<Category> LoadCategoriesList()
        {
            List<Category> categories = new List<Category>();

            using SqlConnection con = new SqlConnection(_configuration.GetConnectionString("SqlCon"));
            con.Open();
            using SqlCommand cmd = new SqlCommand("SELECT Id, Name FROM Categories order by Name", con);
            using SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                Category category = new Category
                {
                    Id = Convert.ToInt32(row["Id"]),
                    Name = row["Name"].ToString()
                };

                categories.Add(category);
            }
            return categories;
        }
    }
}
