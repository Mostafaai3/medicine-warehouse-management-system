﻿using MedicineWarehouse.Domain.Interfaces;
using MedicineWarehouse.Domain.Models;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineWarehouse.Infrastructure.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        private readonly IConfiguration _configuration;

        public UsersRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public Task<List<User>> Get()
        {
            var users = LoadPostsList();
            return Task.FromResult(users.ToList());
        }

        public Task<User> GetById(string id)
        {
            var users = LoadPostsList();
            var user = users.FirstOrDefault(p => p.Id == id);
            return Task.FromResult(user);
        }

        /*public IEnumerable<User> LoadPostsList()
        {
            List<User> posts = new List<User>();
            SqlConnection con = new SqlConnection(_configuration.GetConnectionString("SqlCon"));
            SqlCommand cmd = new SqlCommand("Select * from AspNetUsers", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                User post = new User();
                post.Id = dt.Rows[i]["Id"].ToString();
                post.UserName = dt.Rows[i]["UserName"].ToString();
                posts.Add(post);
            }
            return posts;
        }*/
        public IEnumerable<User> LoadPostsList()
        {
            List<User> users = new List<User>();

            using SqlConnection con = new SqlConnection(_configuration.GetConnectionString("SqlCon"));
            con.Open();
            using SqlCommand cmd = new SqlCommand("SELECT Id, UserName, FirstName, LastName, Mobile, Email FROM AspNetUsers order by FirstName , LastName", con);
            using SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                User user = new User
                {
                    Id = row["Id"].ToString(),
                    UserName = row["UserName"].ToString(),
                    FirstName = row["FirstName"].ToString(),
                    LastName = row["LastName"].ToString(),
                    Mobile = row["Mobile"].ToString(),
                    Email = row["Email"].ToString()
                };

                users.Add(user);
            }
            return users;
        }

    }
}
