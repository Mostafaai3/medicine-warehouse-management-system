﻿using MedicineWarehouse.Domain.DTOs;
using MedicineWarehouse.Domain.Interfaces;
using MedicineWarehouse.Domain.Models;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineWarehouse.Infrastructure.Repositories
{
    public class OrdersRepository : IOrdersRepository
    {
        private readonly IConfiguration _configuration;
        public OrdersRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<string> Add(List<AddItemDto> items,string userId)
        {
            var outParam = new SqlParameter("@ReturnCode", SqlDbType.NVarChar, 20)
            {
                Direction = ParameterDirection.Output
            };
            var createdId = new SqlParameter("@CreatedId", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            SqlParameter[] parameters = {
                new SqlParameter("@UserId",userId),
                outParam,
                createdId
            };

            using (var sqlConnection = new SqlConnection(_configuration.GetConnectionString("SqlCon")))
            {
                await sqlConnection.OpenAsync();
                SqlTransaction transaction = sqlConnection.BeginTransaction();
                int orderId = 0;
                using (var command = sqlConnection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "CreateOrder";
                    command.Transaction = transaction;
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    await command.ExecuteScalarAsync();
                    if (outParam.Value.ToString() != "C200")
                    {
                        return outParam.Value.ToString();
                    }   
                    orderId = Convert.ToInt32(createdId.Value);
                }
                foreach (var item in items)
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        var returnCode = new SqlParameter("@ReturnCode", SqlDbType.NVarChar, 20)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter[] ps = {
                        new SqlParameter("@OrderId",orderId),
                        new SqlParameter("@GroupId",item.GroupId),
                        new SqlParameter("@Quantity",item.Quantity),
                        returnCode
                        };
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "AddItem";
                        command.Transaction = transaction;
                        if (parameters != null)
                        {
                            command.Parameters.AddRange(ps);
                        }
                        await command.ExecuteScalarAsync();
                        if (returnCode.Value.ToString() != "C200")
                        {
                            return returnCode.Value.ToString();
                        }
                    }
                }
                try
                {
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    return "C207";
                }
            }
            return createdId.Value.ToString();
        }

        public async Task<string> ChangeItemState(int groupMedicineId, int stateId)
        {
            var outParam = new SqlParameter("@ReturnCode", SqlDbType.NVarChar, 20)
            {
                Direction = ParameterDirection.Output
            };
            SqlParameter[] parameters = {
                new SqlParameter("@Id",groupMedicineId),
                new SqlParameter("@StateId",stateId),
                outParam
            };
            using (var sqlConnection = new SqlConnection(_configuration.GetConnectionString("SqlCon")))
            {
                using (var command = sqlConnection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ChangeState";
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    await sqlConnection.OpenAsync();
                    await command.ExecuteScalarAsync();
                }
            }
            return outParam.Value.ToString();
        }

        public async Task<string> Delete(int id)
        {
            var outParam = new SqlParameter("@ReturnCode", SqlDbType.NVarChar, 20)
            {
                Direction = ParameterDirection.Output
            };
            SqlParameter[] parameters = {
                new SqlParameter("@Id",id),
                outParam
            };
            using (var sqlConnection = new SqlConnection(_configuration.GetConnectionString("SqlCon")))
            {
                using (var command = sqlConnection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "DeleteOrder";
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    await sqlConnection.OpenAsync();
                    await command.ExecuteScalarAsync();
                }
            }
            return outParam.Value.ToString();
        }

        public async Task<string> DeleteItem(int id)
        {
            var outParam = new SqlParameter("@ReturnCode", SqlDbType.NVarChar, 20)
            {
                Direction = ParameterDirection.Output
            };
            SqlParameter[] parameters = {
                new SqlParameter("@Id",id),
                outParam
            };
            using (var sqlConnection = new SqlConnection(_configuration.GetConnectionString("SqlCon")))
            {
                using (var command = sqlConnection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "DeleteItem";
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    await sqlConnection.OpenAsync();
                    await command.ExecuteScalarAsync();
                }
            }
            return outParam.Value.ToString();
        }

        public Task<List<OrderDetailsDto>> Get(string? userId = null, bool? IsPaid = null, DateTime? startDate = null, DateTime? endDate = null)
        {
            var orders = LoadOrdersList();
            if (userId is not null)
            {
                orders= orders.Where(o => o.UserId.Equals(userId));
            }
            if (IsPaid is not null)
            {
                orders = orders.Where(o => o.IsPaid == IsPaid);
            }
            if (startDate is not null )
            {
                orders = orders.Where(o => DateTime.Compare(startDate.Value , o.Date) <= 0);
            }
            if (endDate is not null)
            {
                orders = orders.Where(o => DateTime.Compare(endDate.Value, o.Date) >= 0);
            }
            return Task.FromResult(orders.ToList());
        }

        public async Task<List<GroupOrderDto>> GetById(int id)
        {
            var orders = LoadOrdersList();
            var order = orders.FirstOrDefault(o => o.Id == id);
            if (order == null) { return null; }
            var items = new List<GroupOrderDto>();
            using (var sqlConnection = new SqlConnection(_configuration.GetConnectionString("SqlCon")))
            {
                using (var command = sqlConnection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetOrderDetails";
                    command.Parameters.Add(new SqlParameter("OrderId", id));
                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        foreach (DataRow row in dt.Rows)
                        {
                            GroupOrderDto item = new GroupOrderDto
                            {
                                Id = Convert.ToInt32(row["Id"]),
                                Price = Convert.ToInt32(row["Price"]),
                                Quantity = Convert.ToInt32(row["Quantity"]),
                                AvailableQuantity = Convert.ToInt32(row["AvailableQuantity"]),
                                OrderId = Convert.ToInt32(row["OrderId"]),
                                GroupId = Convert.ToInt32(row["GroupId"]),
                                MedicineId = Convert.ToInt32(row["MedicineId"]),
                                MedicineName = row["MedicineName"].ToString(),
                                ExpirationDate = Convert.ToDateTime(row["ExpirationDate"])
                            };

                            items.Add(item);
                        }
                    }
                }
            }
            return items;
        }

        public Task<List<State>> GetStates()
        {
            List<State> states = new List<State>();

            using SqlConnection con = new SqlConnection(_configuration.GetConnectionString("SqlCon"));
            con.Open();
            using SqlCommand cmd = new SqlCommand("select * from States", con);
            using SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                State state = new State
                {
                    Id = Convert.ToInt32(row["Id"]),
                    Name = row["Name"].ToString(),
                };

                states.Add(state);
            }
            return Task.FromResult(states);
        }

        public async Task<string> Pay(int orderId)
        {
            var outParam = new SqlParameter("@ReturnCode", SqlDbType.NVarChar, 20)
            {
                Direction = ParameterDirection.Output
            };
            SqlParameter[] parameters = {
                new SqlParameter("@OrderId",orderId),
                outParam
            };
            using (var sqlConnection = new SqlConnection(_configuration.GetConnectionString("SqlCon")))
            {
                using (var command = sqlConnection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "PayOrder";
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    await sqlConnection.OpenAsync();
                    await command.ExecuteScalarAsync();
                }
            }
            return outParam.Value.ToString();
        }

        public IEnumerable<OrderDetailsDto> LoadOrdersList()
        {
            List<OrderDetailsDto> orders = new List<OrderDetailsDto>();

            using SqlConnection con = new SqlConnection(_configuration.GetConnectionString("SqlCon"));
            con.Open();
            using SqlCommand cmd = new SqlCommand("execute GetOrders", con);
            using SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                OrderDetailsDto order = new OrderDetailsDto
                {
                    Id = Convert.ToInt32(row["Id"]),
                    UserId = row["UserId"].ToString(),
                    IsPaid = Convert.ToBoolean(row["IsPaid"]),
                    UserName = row["UserName"].ToString(),
                    Date = Convert.ToDateTime(row["Date"])
                };

                orders.Add(order);
            }
            return orders;
        }
    }
}
