﻿using MedicineWarehouse.Domain.Interfaces;
using MedicineWarehouse.Domain.Models;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineWarehouse.Infrastructure.Repositories
{
    public class ScientificNamesRepository : IScientificNamesRepository
    {
        private readonly IConfiguration _configuration;
        public ScientificNamesRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task<string> Save(ScientificName scientificName)
        {
            var outParam = new SqlParameter("@ReturnCode", SqlDbType.NVarChar, 20)
            {
                Direction = ParameterDirection.Output
            };
            SqlParameter[] parameters = {
                new SqlParameter("@Id",scientificName.Id),
                new SqlParameter("@Name",scientificName.Name),
                outParam
            };
            using (var sqlConnection = new SqlConnection(_configuration.GetConnectionString("SqlCon")))
            {
                using (var command = sqlConnection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "SaveScientificName";
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    await sqlConnection.OpenAsync();
                    await command.ExecuteScalarAsync();
                }
            }
            return outParam.Value.ToString();
        }

        public async Task<string> Delete(int id)
        {
            var outParam = new SqlParameter("@ReturnCode", SqlDbType.NVarChar, 20)
            {
                Direction = ParameterDirection.Output
            };
            SqlParameter[] parameters = {
                new SqlParameter("@Id",id),
                outParam
            };
            using (var sqlConnection = new SqlConnection(_configuration.GetConnectionString("SqlCon")))
            {
                using (var command = sqlConnection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "DeleteScientificName";
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    await sqlConnection.OpenAsync();
                    await command.ExecuteScalarAsync();
                }
            }
            return outParam.Value.ToString();
        }

        public Task<List<ScientificName>> Get()
        {
            var scientificNames = LoadScientificNamesList();
            return Task.FromResult(scientificNames.ToList());
        }

        public IEnumerable<ScientificName> LoadScientificNamesList()
        {
            List<ScientificName> scientificNames = new List<ScientificName>();

            using SqlConnection con = new SqlConnection(_configuration.GetConnectionString("SqlCon"));
            con.Open();
            using SqlCommand cmd = new SqlCommand("SELECT Id, Name FROM ScientificNames order by Name", con);
            using SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                ScientificName scientificName = new ScientificName
                {
                    Id = Convert.ToInt32(row["Id"]),
                    Name = row["Name"].ToString()
                };

                scientificNames.Add(scientificName);
            }
            return scientificNames;
        }
    }
}
