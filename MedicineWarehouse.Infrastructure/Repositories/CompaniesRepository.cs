﻿using MedicineWarehouse.Domain.Interfaces;
using MedicineWarehouse.Domain.Models;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineWarehouse.Infrastructure.Repositories
{
    public class CompaniesRepository : ICompaniesRepository
    {
        private readonly IConfiguration _configuration;
        public CompaniesRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task<string> Delete(int id)
        {
            var outParam = new SqlParameter("@ReturnCode", SqlDbType.NVarChar, 20)
            {
                Direction = ParameterDirection.Output
            };
            SqlParameter[] parameters = {
                new SqlParameter("@Id",id),
                outParam
            };
            using (var sqlConnection = new SqlConnection(_configuration.GetConnectionString("SqlCon")))
            {
                using (var command = sqlConnection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "DeleteCompany";
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    await sqlConnection.OpenAsync();
                    await command.ExecuteScalarAsync();
                }
            }
            return outParam.Value.ToString();
        }

        public Task<List<Company>> Get()
        {
            var companies = LoadCompaniesList();
            return Task.FromResult(companies.ToList());
        }

        public async Task<string> Save(Company company)
        {
            var outParam = new SqlParameter("@ReturnCode", SqlDbType.NVarChar, 20)
            {
                Direction = ParameterDirection.Output
            };
            SqlParameter[] parameters = {
                new SqlParameter("@Id",company.Id),
                new SqlParameter("@Name",company.Name),
                outParam
            };
            using (var sqlConnection = new SqlConnection(_configuration.GetConnectionString("SqlCon")))
            {
                using (var command = sqlConnection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "SaveCompany";
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    await sqlConnection.OpenAsync();
                    await command.ExecuteScalarAsync();
                }
            }
            return outParam.Value.ToString();
        }

        public IEnumerable<Company> LoadCompaniesList()
        {
            List<Company> companies = new List<Company>();

            using SqlConnection con = new SqlConnection(_configuration.GetConnectionString("SqlCon"));
            con.Open();
            using SqlCommand cmd = new SqlCommand("SELECT Id, Name FROM Companies order by Name", con);
            using SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                Company company = new Company
                {
                    Id = Convert.ToInt32(row["Id"]),
                    Name = row["Name"].ToString()
                };

                companies.Add(company);
            }
            return companies;
        }
    }
}
