﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineWarehouse.Domain.Models
{
    public class Group
    {
        public int Id { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int Quantity { get; set; }
        public int MedicineId { get; set; }
    }
}
