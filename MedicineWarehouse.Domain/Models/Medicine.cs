﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineWarehouse.Domain.Models
{
    public class Medicine
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public string Barcode { get; set; }
        public int CompanyId { get; set; }
        public int CategoryId { get; set; }
        public int ScientificNameId { get; set; }
    }
}
