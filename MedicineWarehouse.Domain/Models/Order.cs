﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineWarehouse.Domain.Models
{
    public class Order
    {
        public int Id { get; set; }
        public bool IsPaid { get; set; }
        public string UserId { get; set; }
        public DateTime Date { get; set; }
    }
}
