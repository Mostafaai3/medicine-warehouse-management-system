﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MedicineWarehouse.Domain.Models
{
    public class Message<T>
    {
        public bool IsSuccess { get; set; }

        public string ReturnMessage { get; set; }

        public T Data { get; set; }
    }
}
