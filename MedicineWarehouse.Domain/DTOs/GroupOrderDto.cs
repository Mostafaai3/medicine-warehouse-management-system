﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineWarehouse.Domain.DTOs
{
    public class GroupOrderDto
    {
        public int Id { get; set; }
        public int MedicineId { get; set; }
        public string MedicineName { get; set; }
        public int OrderId { get; set; }
        public int GroupId { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int Quantity { get; set; }
        public int? Price { get; set; }
        public int AvailableQuantity { get; set; }
    }
}
