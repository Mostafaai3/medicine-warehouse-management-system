﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineWarehouse.Domain.DTOs
{
    public class RegisterDto
    {
        [Required, StringLength(50)]
        public string FirstName { get; set; }

        [Required, StringLength(50)]
        public string LastName { get; set; }

        [Required, StringLength(50)]
        public string UserName { get; set; }

        [StringLength(500), EmailAddress]
        public string Email { get; set; }

        [Required, StringLength(100)]
        public string Password { get; set; }

        [Required, RegularExpression("^[0-9]{14}$", ErrorMessage = "Phone number must be 14 digits")]
        public string Mobile { get; set; }
    }
}
