﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineWarehouse.Domain.DTOs
{
    public class AddItemDto
    {
        public int GroupId { get; set; }
        public int Quantity { get; set; }
    }
}
