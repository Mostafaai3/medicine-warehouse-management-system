﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineWarehouse.Domain.DTOs
{
    public class MedicineDetailsDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public string Barcode { get; set; }
        public int CompanyId { get; set; }
        public string Company { get; set; }
        public int CategoryId { get; set; }
        public string Category { get; set; }
        public int ScientificNameId { get; set; }
        public string ScientificName { get; set; }
    }
}
