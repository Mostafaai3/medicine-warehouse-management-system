﻿using MedicineWarehouse.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineWarehouse.Domain.DTOs
{
    public class AddMedicineFormDto
    {
        public IEnumerable<Category> Categories { get; set; }
        public IEnumerable<Company> Companies { get; set; }
        public IEnumerable<ScientificName> ScientificNames { get; set; }
    }
}
