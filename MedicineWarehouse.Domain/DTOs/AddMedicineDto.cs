﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineWarehouse.Domain.DTOs
{
    public class AddMedicineDto
    {
        [Required, StringLength(100)]
        public string Name { get; set; }
        public int Price { get; set; }
        public string Barcode { get; set; }
        public int CompanyId { get; set; }
        [Required]
        public int CategoryId { get; set; }
        public int ScientificNameId { get; set; }
    }
}
