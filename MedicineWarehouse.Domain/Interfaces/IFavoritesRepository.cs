﻿using MedicineWarehouse.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineWarehouse.Domain.Interfaces
{
    public interface IFavoritesRepository
    {
        Task<List<Medicine>> Get();
        Task<Medicine> GetById(int id);
        Task<string> Add();
        Task<string> Delete(int id);
        Task<string> Update();
    }
}
