﻿using MedicineWarehouse.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineWarehouse.Domain.Interfaces
{
    public interface ICategoriesRepository
    {
        Task<List<Category>> Get();
        Task<string> Save(Category category);
        Task<string> Delete(int id);
    }
}
