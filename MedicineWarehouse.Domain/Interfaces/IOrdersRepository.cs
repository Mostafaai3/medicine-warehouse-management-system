﻿using MedicineWarehouse.Domain.DTOs;
using MedicineWarehouse.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineWarehouse.Domain.Interfaces
{
    public interface IOrdersRepository
    {
        Task<List<OrderDetailsDto>> Get(string? userId = null, bool? IsPaid = null, DateTime? startDate= null, DateTime? endDate=null);
        Task<List<GroupOrderDto>> GetById(int id);
        Task<string> Add(List<AddItemDto> groups, string userId);
        Task<string> Delete(int id);
        Task<string> DeleteItem(int id);
        Task<List<State>> GetStates();
        Task<string> ChangeItemState(int groupMedicineId, int stateId);
        Task<string> Pay(int orderId);
    }
}
