﻿using MedicineWarehouse.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineWarehouse.Domain.Interfaces
{
    public interface IUsersRepository
    {
        Task<List<User>> Get();
        Task<User> GetById(string id);
    }
}
