﻿using MedicineWarehouse.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineWarehouse.Domain.Interfaces
{
    public interface IScientificNamesRepository
    {
        Task<List<ScientificName>> Get();
        Task<string> Save(ScientificName scientificName);
        Task<string> Delete(int id);
    }
}
