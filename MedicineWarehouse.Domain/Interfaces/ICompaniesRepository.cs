﻿using MedicineWarehouse.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineWarehouse.Domain.Interfaces
{
    public interface ICompaniesRepository
    {
        Task<List<Company>> Get();
        Task<string> Save(Company company);
        Task<string> Delete(int id);
    }
}
