﻿using MedicineWarehouse.Domain.DTOs;
using MedicineWarehouse.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicineWarehouse.Domain.Interfaces
{
    public interface IMedicinesRepository
    {
        Task<List<MedicineDetailsDto>> Get();
        Task<List<MedicineDetailsDto>> Search(string? term = null , int? categoryId = null, int? companyId = null, int? scientificNameId = null);
        Task<MedicineDetailsDto> GetById(int id);
        Task<MedicineDetailsDto> GetByCode(string code);
        Task<string> Save(Medicine medicine);
        Task<string> Delete(int id);
        Task<List<Group>> GetMedicineGroups(int id);
        Task<string> SupplyMedicine(SupplyMedicineDto dto);
    }
}
