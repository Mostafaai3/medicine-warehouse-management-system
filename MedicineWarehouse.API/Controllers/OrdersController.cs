﻿using MedicineWarehouse.Domain.DTOs;
using MedicineWarehouse.Domain.Interfaces;
using MedicineWarehouse.Domain.Models;
using MedicineWarehouse.Infrastructure.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace MedicineWarehouse.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IOrdersRepository _ordersRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public OrdersController(IOrdersRepository ordersRepository, IHttpContextAccessor httpContextAccessor)
        {
            _ordersRepository = ordersRepository;
            _httpContextAccessor = httpContextAccessor;
        }

        [Authorize]
        [HttpGet("GetOrderDetails/{id}")]
        public async Task<ActionResult<IEnumerable<GroupOrderDto>>> GetOrderDetails(int id)
        {
            var result = await _ordersRepository.GetById(id);
            if (result == null) { return NotFound("Order not found"); }
            return Ok(result);
        }

        [Authorize]
        [HttpGet("GetOrders")]
        public async Task<ActionResult<IEnumerable<OrderDetailsDto>>> GetOrders(string? userId = null, bool? IsPaid = null, DateTime? startDate = null, DateTime? endDate = null)
        {
            var orders = await _ordersRepository.Get(userId, IsPaid, startDate, endDate);
            return Ok(orders);
        }

        [Authorize]
        [HttpPost("AddOrder")]
        public async Task<ActionResult> AddOrder(List<AddItemDto> items)
        {
            var userName = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var result = await _ordersRepository.Add(items,userName);
            if (result == "C203")
            {
                return BadRequest();
            }
            if (result == "C204")
            {
                return BadRequest();
            }
            if (result == "C205")
            {
                return BadRequest("Group not found");
            }

            if (result == "C207")
            {
                return BadRequest();
            }
            int orderId = Convert.ToInt32(result);
            return CreatedAtAction("GetOrderDetails", new { id = orderId }, new {id = orderId});
        }

        [Authorize]
        [HttpGet("GetStates")]
        public async Task<ActionResult<IEnumerable<State>>> GetStates()
        {
            var states = await _ordersRepository.GetStates();
            return Ok(states);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("ChangeState")]
        public async Task<ActionResult> ChangeState(int id, int StateId)
        {
            var result = await _ordersRepository.ChangeItemState(id, StateId);
            if (result == "C203")
            {
                return NotFound("Item not found");
            }
            if (result == "C204")
            {
                return BadRequest("State not found");
            }
            if (result == "C205")
            {
                return BadRequest("State already assigned");
            }
            if (result == "C206")
            {
                return BadRequest("No enough quantity");
            }
            if (result == "C200")
            {
                return Ok("Changed successfully");
            }
            return BadRequest();
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("PayOrder/{id}")]
        public async Task<ActionResult> PayOrder(int id)
        {
            if (await _ordersRepository.Pay(id) == "C200")
            {
                return Ok("Payed successfully");
            }
            return NotFound();
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("DeleteOrder/{id}")]
        public async Task<ActionResult> DeleteOrder(int id)
        {
            if (await _ordersRepository.Delete(id) == "C200")
            {
                return Ok("Deleted successfully");
            }
            return NotFound();
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("DeleteItem/{id}")]
        public async Task<ActionResult> DeleteItem(int id)
        {
            if (await _ordersRepository.DeleteItem(id) == "C200")
            {
                return Ok("Deleted successfully");
            }
            return NotFound();
        }
    }
}
