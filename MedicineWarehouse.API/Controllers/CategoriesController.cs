﻿using MedicineWarehouse.Domain.Interfaces;
using MedicineWarehouse.Domain.Models;
using MedicineWarehouse.Infrastructure.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace MedicineWarehouse.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategoriesRepository _categoriesRepository;
        public CategoriesController(ICategoriesRepository categoriesRepository)
        {
            _categoriesRepository = categoriesRepository;
        }

        [Authorize]
        [HttpGet("GetCategories")]
        public async Task<ActionResult<IEnumerable<Category>>> GetCategories()
        {
            var categories = await _categoriesRepository.Get();
            return Ok(categories);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("AddCategory")]
        public async Task<ActionResult> AddCategory(string name)
        {
            var category = new Category
            {
                Id = 0,
                Name = name
            };

            if (await _categoriesRepository.Save(category) == "C200")
            {
                return Ok("Added successfully");
            }
            return BadRequest("Category already exist");
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("UpdateCategory")]
        public async Task<ActionResult> UpdateCategory(Category category)
        {
            if (category.Id == 0)
            {
                return NotFound("Category not found");
            }
            var result = await _categoriesRepository.Save(category);
            if (result == "C201")
            {
                return BadRequest("Category already exist");
            }
            if (result == "C203")
            {
                return NotFound("Category not found");
            }
            if (result == "C200")
            {
                return Ok("Updated successfully");
            }
            return BadRequest();
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("DeleteCategory/{id}")]
        public async Task<ActionResult> DeleteCategory(int id)
        {
            if (await _categoriesRepository.Delete(id) == "C200")
            {
                return Ok("Deleted successfully");
            }
            return NotFound();
        }
    }
}
