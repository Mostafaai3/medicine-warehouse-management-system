﻿using MedicineWarehouse.Domain.DTOs;
using MedicineWarehouse.Domain.Interfaces;
using MedicineWarehouse.Domain.Models;
using MedicineWarehouse.Infrastructure.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace MedicineWarehouse.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MedicinesController : ControllerBase
    {
        private readonly IMedicinesRepository _medicinesRepository;
        private readonly ICategoriesRepository _categoriesRepository;
        private readonly ICompaniesRepository _companiesRepository;
        private readonly IScientificNamesRepository _scientificNamesRepository;
        public MedicinesController(IMedicinesRepository medicinesRepository, IScientificNamesRepository scientificNamesRepository, ICompaniesRepository companiesRepository, ICategoriesRepository categoriesRepository)
        {
            _medicinesRepository = medicinesRepository;
            _categoriesRepository = categoriesRepository;
            _companiesRepository = companiesRepository;
            _scientificNamesRepository = scientificNamesRepository;
        }

        [Authorize]
        [HttpGet("GetMedicines")]
        public async Task<ActionResult<IEnumerable<MedicineDetailsDto>>> GetMedicines()
        {
            var medicines = await _medicinesRepository.Get();
            return Ok(medicines);
        }

        [Authorize]
        [HttpGet("GetMedicine/{id}")]
        public async Task<ActionResult<MedicineDetailsDto>> GetMedicine(int id)
        {
            var medicine = await _medicinesRepository.GetById(id);
            return Ok(medicine);
        }

        [Authorize]
        [HttpGet("GetForm")]
        public async Task<ActionResult<AddMedicineFormDto>> GetForm()
        {
            var categories = await _categoriesRepository.Get();
            var companies = await _companiesRepository.Get();
            var scientificNames = await _scientificNamesRepository.Get();
            var form = new AddMedicineFormDto
            {
                Categories = categories,
                Companies = companies,
                ScientificNames = scientificNames
            };
            return Ok(form);
        }

        [Authorize]
        [HttpGet("GetMedicineByCode")]
        public async Task<ActionResult<MedicineDetailsDto>> GetMedicineByCode(string code)
        {
            var medicine = await _medicinesRepository.GetByCode(code);
            return Ok(medicine);
        }

        [Authorize]
        [HttpGet("SearchMedicine")]
        public async Task<ActionResult<IEnumerable<MedicineDetailsDto>>> SearchMedicine(string? term = null, int? categoryId = null, int? companyId = null, int? scientificNameId = null)
        {
            var medicines = await _medicinesRepository.Search(term,categoryId,companyId,scientificNameId);
            return Ok(medicines);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("AddMedicine")]
        public async Task<ActionResult> AddMedicine(AddMedicineDto dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var medicine = new Medicine
            {
                Id = 0,
                Name = dto.Name,
                Price = dto.Price,
                Barcode = dto.Barcode,
                ScientificNameId = dto.ScientificNameId,
                CategoryId = dto.CategoryId,
                CompanyId = dto.CompanyId,
            };

            var result = await _medicinesRepository.Save(medicine);

            if (result == "C204")
            {
                return BadRequest("Company not found");
            }
            if (result == "C205")
            {
                return BadRequest("Category not found");
            }
            if (result == "C206")
            {
                return BadRequest("ScientificName not found");
            }
            if (result == "C200")
            {
                return Ok("Added successfully");
            }
            return BadRequest();
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("UpdateMedicine")]
        public async Task<ActionResult> UpdateMedicine(Medicine medicine)
        {
            if (medicine.Id == 0)
            {
                return NotFound("Medicine not found");
            }
            var result = await _medicinesRepository.Save(medicine);
            if (result == "C203")
            {
                return NotFound("Medicine not found");
            }
            if (result == "C204")
            {
                return BadRequest("Company not found");
            }
            if (result == "C205")
            {
                return BadRequest("Category not found");
            }
            if (result == "C206")
            {
                return BadRequest("ScientificName not found");
            }
            if (result == "C200")
            {
                return Ok("Updated successfully");
            }
            return BadRequest();
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("DeleteMedicine/{id}")]
        public async Task<ActionResult> DeleteMedicine(int id)
        {
            if (await _medicinesRepository.Delete(id) == "C200")
            {
                return Ok("Deleted successfully");
            }
            return NotFound();
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("SupplyMedicine")]
        public async Task<ActionResult> SupplyMedicine(SupplyMedicineDto dto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var result = await _medicinesRepository.SupplyMedicine(dto);
            if (result == "C201")
            {
                return BadRequest("Medicine not found");
            }
            return Ok("Added successfully");
        }

        [Authorize]
        [HttpGet("GetMedicineGroups/{medicineId}")]
        public async Task<ActionResult<IEnumerable<Group>>> GetMedicineGroups(int medicineId)
        {
            var medicine = _medicinesRepository.GetById(medicineId);
            if (medicine.Result == null) return BadRequest("Medicine not found");
            var result = await _medicinesRepository.GetMedicineGroups(medicineId);
            return Ok(result);
        }
    }
}
