﻿using MedicineWarehouse.Domain.Interfaces;
using MedicineWarehouse.Domain.Models;
using MedicineWarehouse.Infrastructure.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MedicineWarehouse.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ScientificNamesController : ControllerBase
    {
        private readonly IScientificNamesRepository _scientificNamesRepository;
        public ScientificNamesController(IScientificNamesRepository scientificNamesRepository)
        {
            _scientificNamesRepository = scientificNamesRepository;
        }

        [Authorize]
        [HttpGet("GetScientificNames")]
        public async Task<ActionResult<IEnumerable<ScientificName>>> GetScientificNames()
        {
            var scientificNames = await _scientificNamesRepository.Get();
            return Ok(scientificNames);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("AddScientificName")]
        public async Task<ActionResult> AddScientificName(string name)
        {
            var scientificName = new ScientificName
            {
                Id = 0,
                Name = name
            };
            
            if (await _scientificNamesRepository.Save(scientificName) == "C200")
            {
                return Ok("Added successfully");
            }
            return BadRequest("ScientificNames already exist");
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("UpdateScientificName")]
        public async Task<ActionResult> UpdateScientificName(ScientificName scientificName)
        {
            if (scientificName.Id == 0)
            {
                return NotFound("ScientificName not found");
            }
            var result = await _scientificNamesRepository.Save(scientificName);
            if (result == "C201")
            {
                return BadRequest("ScientificName already exist");
            }
            if (result == "C203")
            {
                return NotFound("ScientificName not found");
            }
            if (result == "C200")
            {
                return Ok("Updated successfully");
            }
            return BadRequest();
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("DeleteScientificName/{id}")]
        public async Task<ActionResult> DeleteScientificName(int id)
        {
            if (await _scientificNamesRepository.Delete(id) == "C200")
            {
                return Ok("Deleted successfully");
            }
            return NotFound();
        }

    }
}
