﻿using MedicineWarehouse.Domain.Interfaces;
using MedicineWarehouse.Domain.Models;
using MedicineWarehouse.Infrastructure.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MedicineWarehouse.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompaniesController : ControllerBase
    {
        private readonly ICompaniesRepository _companiesRepository;
        public CompaniesController(ICompaniesRepository companiesRepository)
        {
            _companiesRepository = companiesRepository;
        }

        [Authorize]
        [HttpGet("GetCompanies")]
        public async Task<ActionResult<IEnumerable<Company>>> GetCompanies()
        {
            var companies = await _companiesRepository.Get();
            return Ok(companies);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("AddCompany")]
        public async Task<ActionResult> AddCompany(string name)
        {
            var company = new Company
            {
                Id = 0,
                Name = name
            };

            if (await _companiesRepository.Save(company) == "C200")
            {
                return Ok("Added successfully");
            }
            return BadRequest("Company already exist");
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("UpdateCompany")]
        public async Task<ActionResult> UpdateCompany(Company company)
        {
            if (company.Id == 0)
            {
                return NotFound("Company not found");
            }
            var result = await _companiesRepository.Save(company);
            if (result == "C201")
            {
                return BadRequest("Company already exist");
            }
            if (result == "C203")
            {
                return NotFound("Company not found");
            }
            if (result == "C200")
            {
                return Ok("Updated successfully");
            }
            return BadRequest();
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("DeleteCompany/{id}")]
        public async Task<ActionResult> DeleteCompany(int id)
        {
            if (await _companiesRepository.Delete(id) == "C200")
            {
                return Ok("Deleted successfully");
            }
            return NotFound();
        }
    }
}
